function collisionCheck(Hero)
{
    // A for loop for going through each of the squares!
    for (var i = 0; i < obstacleArray.length; i++)
    {
        //Let's use these variables to get all the info about our squares
        //The hero
        var HeroTop = Hero.y;
        //console.log(Hero.y);
        var HeroBot = Hero.y + Hero.height;
        //console.log(Hero.height);
        var HeroLef = Hero.x;
        //console.log(Hero.x);
        var HeroRit = Hero.x + Hero.width;
        //console.log(Hero.width);

        //The wasll we're on in the for loop
        var WallTop = obstacleArray[i].y;
        //console.log(WallTop);
        var WallBot = obstacleArray[i].y + obstacleArray[i].height;
        //console.log(WallBot);
        var WallLef = obstacleArray[i].x;
        //console.log(WallLef);
        var WallRit = obstacleArray[i].x + obstacleArray[i].width;
        //console.log(WallRit);

        // So, we have 4 boolean statements in our if!
        // See these '||' symbols? It's called an 'or'
        // It means that when we look at multiple boolean statements, if any one of them is true, then they all become true!
        // So, we look at out boolean statements
        // We check to see if any of them are true!
        // If none of them are true, then that means they are all false
        // If they are all false, then there is collision! So, GO BACK!
        if ((HeroTop >= WallBot || HeroBot <= WallTop || HeroRit <= WallLef || HeroLef >= WallRit) == false)
        {
            if (obstacleArray[i].width == 40)
            {
                score -= 100;
                obstacleArray[i] = new Rock1(deadLine, obstacleArray[i].y);
            }
            if (obstacleArray[i].width == 120)
            {
                score -= 300;
                obstacleArray[i] = new Rock2(deadLine, obstacleArray[i].y);
            }
            playRockSound();
        }
    }

    for (var i = 0; i < diamondArray.length; i++)
    {
        //Let's use these variables to get all the info about our squares
        //The hero
        var HeroTop = Hero.y;
        //console.log(Hero.y);
        var HeroBot = Hero.y + Hero.height;
        //console.log(Hero.height);
        var HeroLef = Hero.x;
        //console.log(Hero.x);
        var HeroRit = Hero.x + Hero.width;
        //console.log(Hero.width);

        //The wasll we're on in the for loop
        var DiamondTop = diamondArray[i].y;
        //console.log(WallTop);
        var DiamondBot = diamondArray[i].y + diamondArray[i].height;
        //console.log(WallBot);
        var DiamondLef = diamondArray[i].x;
        //console.log(WallLef);
        var DiamondRit = diamondArray[i].x + diamondArray[i].width;
        //console.log(WallRit);

        // So, we have 4 boolean statements in our if!
        // See these '||' symbols? It's called an 'or'
        // It means that when we look at multiple boolean statements, if any one of them is true, then they all become true!
        // So, we look at out boolean statements
        // We check to see if any of them are true!
        // If none of them are true, then that means they are all false
        // If they are all false, then there is collision! So, GO BACK!
        if ((HeroTop >= DiamondBot || HeroBot <= DiamondTop || HeroRit <= DiamondLef || HeroLef >= DiamondRit) == false)
        {
            gem += 1;
            diamondArray[i] = new Diamond(deadLine, diamondArray[i].y);
            playDiamondSound();
        }
    }
}
