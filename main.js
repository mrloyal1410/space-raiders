function startGame()
{
    gameInit(level);
    playBackgroundSound();
    loop();
}

function gameInit(newLevel)
{
    level = newLevel;
    console.log("level: " + level);
    wallHeight = getLevelWallHeight(newLevel);
    console.log("wallHeight: " + wallHeight);
    distance = getLevelDistance(newLevel);
    console.log("distance: " + distance);
    speed = getLevelSpeed(newLevel);
    console.log("speed: " + speed);
    rocks = getLevelRocks(newLevel);
    console.log("rocks: " + rocks);
    diamonds = getLevelDiamonds(newLevel);
    console.log("diamonds: " + diamonds);

    fillGameContent();

    for (var i = 0; i < rocks; i++)
    {
        var randomnumber = Math.floor(Math.random() * (canvas.height - (2 * wallHeight) - 120)) + wallHeight;
        if (randomnumber % 2 == 0)
        {
            obstacleArray[i] = new Rock1(canvas.width + (i * distance), randomnumber);
            console.log("Rock1 " + i + ": " + obstacleArray[i].x + "-----" + obstacleArray[i].width);
        }
        if (randomnumber % 2 == 1)
        {
            obstacleArray[i] = new Rock2(canvas.width + (i * distance), randomnumber);
            console.log("Rock2 " + i + ": " + obstacleArray[i].x + "-----" + obstacleArray[i].width);
        }
    }

    for (var i = 0; i < diamonds; i++)
    {
        var randomnumber = Math.floor(Math.random() * (canvas.height - (2 * wallHeight) - 58)) + wallHeight;
        diamondArray[i] = new Diamond(canvas.width + (i * distance), randomnumber);
        console.log("Diamond " + i + ": " + diamondArray[i].x + "-----" + diamondArray[i].width);
    }
}

function loop()
{
    gameInterval = setInterval(function()
    {
        collisionCheck(human);

        star1 = animateBackgroundImage(star1);
        star2 = animateBackgroundImage(star2);
        // reset star x
        if (star1.x < (star1StartX - star1.width))
        {
            star1.x = star1StartX;
        }
        if (star2.x < (star2StartX - star2.width))
        {
            star2.x = star2StartX;
        }

        // clear
        refreshGameCanvas();

        human = animateHero(human);
        // gravity
        human.y += 0.95;
        human.draw();

        for (var i = 0; i < rocks; i++)
        {
            obstacleArray[i] = animateRock(obstacleArray[i]);
            obstacleArray[i].draw();
            /*if (i == 1)
             {
             console.log("Rock " + i + ": " + obstacleArray[i].x);
             }*/
        }

        for (var i = 0; i < diamonds; i++)
        {
            diamondArray[i] = animateRock(diamondArray[i]);
            diamondArray[i].draw();
            /*if (i == 1)
             {
             console.log("Diamond " + i + ": " + diamondArray[i].x);
             }*/
        }

        score += 1;
        refreshInfoCanvas();

        if (obstacleArray[rocks - 1].x <= deadLine)
        {
            stopGame();
        }

        if (score < 0)
        {
            gameOver();
        }
    }, 1000 / 60);
}

function nextLevel()
{
    gameInit(level + 1);
    loop();
    refreshInfoCanvas();
}

function stopGame()
{
    clearInterval(gameInterval);
    refreshGameCanvas();
}

function gameOver()
{
    stopGame();
    playGameOverSound();
    setTimeout(function()
    {
        //alert("Game Over !");
        window.location = "game-over.html"
    }, 4000);
}

function drawRectangle(currentContext, x, y, width, height, fill)
{
    currentContext.fillStyle = fill;
    currentContext.fillRect(x, y, width, height);
}

function undrawObstacleObject(obstacleObject)
{
    context.clearRect(obstacleObject.x, obstacleObject.y, obstacleObject.width, obstacleObject.height);
}

function Hero(x, y)
{
    this.x = x;
    var xx = this.x;
    this.y = y;
    var yy = this.y;
    this.width = 70;
    this.height = 32;
    this.imageObject = new Image();
    this.imageObject.src = "enemy_ship_1.png";
    this.draw = function()
    {
        context.drawImage(this.imageObject, xx, yy, this.width, this.height);
        //console.log("this.x = " + xx);
    };
}

function Rock1(x, y)
{
    this.x = x;
    var xx = this.x;
    this.y = y;
    var yy = this.y;
    this.width = 40;
    this.height = 40;
    this.imageObject = new Image();
    this.imageObject.src = "rock1.png";
    this.draw = function()
    {
        context.drawImage(this.imageObject, xx, yy, this.width, this.height);
        //console.log("this.x = " + xx);
    };
}

function Rock2(x, y)
{
    this.x = x;
    var xx = this.x;
    this.y = y;
    var yy = this.y;
    this.width = 120;
    this.height = 120;
    this.imageObject = new Image();
    this.imageObject.src = "rock2.png";
    this.draw = function()
    {
        context.drawImage(this.imageObject, xx, yy, this.width, this.height);
        //console.log("this.x = " + xx);
    };
}

function Diamond(x, y)
{
    this.x = x;
    var xx = this.x;
    this.y = y;
    var yy = this.y;
    this.width = 55;
    this.height = 58;
    this.imageObject = new Image();
    this.imageObject.src = "diamond.png";
    this.draw = function()
    {
        context.drawImage(this.imageObject, xx, yy, this.width, this.height);
        //console.log("this.x = " + xx);
    };
}

function BackgroundImage(x, y)
{
    this.x = x;
    var xx = this.x;
    this.y = y;
    var yy = this.y;
    this.width = 2400;
    this.height = 600;
    this.imageObject = new Image();
    this.imageObject.src = "star.png";
    this.draw = function()
    {
        context.drawImage(this.imageObject, xx, yy, this.width, this.height);
        //console.log("this.x = " + xx);
    };
}
