function refreshGameCanvas()
{
    context.clearRect(0, 0, canvas.width, canvas.height);
    star1.draw();
    star2.draw();
    fillGameContent();
}

function fillGameContent()
{
    var topWall = new drawRectangle(context, 0, 0, canvas.width, wallHeight, "black");
    var bottomWall = new drawRectangle(context, 0, canvas.height - wallHeight, canvas.width, wallHeight, "black");
}

function refreshInfoCanvas()
{
    contextInfo.clearRect(0, 0, canvasInfo.width, canvasInfo.height);
    fillInfoContent();
}

function fillInfoContent()
{
    contextInfo.font = '32pt Calibri';
    contextInfo.textAlign = 'center';
    contextInfo.textBaseline = 'middle';
    contextInfo.fillStyle = 'blue';
    contextInfo.fillText("Level " + level, canvasInfo.width / 4, canvasInfo.height / 2);
    if (score >= 0)
    {
        contextInfo.fillText("Score " + score, canvasInfo.width / 4 * 2, canvasInfo.height / 2);
    }
    if (score < 0)
    {
        contextInfo.fillText("Score 0", canvasInfo.width / 4 * 2, canvasInfo.height / 2);
    }
    contextInfo.fillText("Gem " + gem, canvasInfo.width / 4 * 3, canvasInfo.height / 2);
}

function playBackgroundSound()
{
    document.getElementById("bg-sound").play();
}

function playRockSound()
{
    document.getElementById("rock-sound").play();
}

function playDiamondSound()
{
    document.getElementById("diamond-sound").play();
}

function playGameOverSound()
{
    document.getElementById("game-over-sound").play();
}

function soundOn()
{
    document.getElementById("bg-sound").volume = 1.0;
    document.getElementById("rock-sound").volume = 1.0;
    document.getElementById("diamond-sound").volume = 1.0;
    document.getElementById("game-over-sound").volume = 1.0;
}

function soundOff()
{
    document.getElementById("bg-sound").volume = 0.0;
    document.getElementById("rock-sound").volume = 0.0;
    document.getElementById("diamond-sound").volume = 0.0;
    document.getElementById("game-over-sound").volume = 0.0;
}
