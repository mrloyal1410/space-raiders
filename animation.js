function animateRock(rock)
{
    // change x
    if (rock.x >= deadLine)
    {
        rock.x -= speed;
    }

    // update x
    var newRockX = rock.x;
    var newRockY = rock.y;
    if (rock.width == 40)
    {
        rock = new Rock1(newRockX, newRockY);
    }
    if (rock.width == 120)
    {
        rock = new Rock2(newRockX, newRockY);
    }
    if (rock.width == 55)
    {
        rock = new Diamond(newRockX, newRockY);
    }
    return rock;
}

function animateBackgroundImage(backgroundImage)
{
    // change x
    backgroundImage.x -= speed * 4;
    //console.log(backgroundImage.x);

    // update x
    var newBackgroundImageX = backgroundImage.x;
    var newBackgroundImageY = backgroundImage.y;
    backgroundImage = new BackgroundImage(newBackgroundImageX, newBackgroundImageY);
    return backgroundImage;
}

function animateHero(hero)
{
    // check the keys and do the movement.
    if (keys[38])
    {
        if (velY > -spaceshipSpeed)
        {
            velY--;
        }
    }
    if (keys[40])
    {
        if (velY < spaceshipSpeed)
        {
            velY++;
        }
    }
    if (keys[39])
    {
        if (velX < spaceshipSpeed)
        {
            velX++;
        }
    }
    if (keys[37])
    {
        if (velX > -spaceshipSpeed)
        {
            velX--;
        }
    }

    // apply some friction to x velocity.
    velX *= friction;
    hero.x += velX;

    // apply some friction to y velocity.
    velY *= friction;
    hero.y += velY;

    // bounds checking
    if (hero.x >= canvas.width - hero.width)
    {
        hero.x = canvas.width - hero.width;
    }
    if (hero.x <= 0)
    {
        hero.x = 0;
    }
    if (hero.y > canvas.height - wallHeight - hero.height)
    {
        hero.y = canvas.height - wallHeight - hero.height;
    }
    if (hero.y <= wallHeight)
    {
        hero.y = wallHeight;
    }

    var heroX = hero.x;
    var heroY = hero.y;
    hero = new Hero(heroX, heroY);
    return hero;
}
