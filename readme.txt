Intel Ultimate Engineering Experience 2013 - PCC Rock Creek
Team 6 Members: Tony Tran, Eric Tsai, Daniel Or, Salad, Collin.

Space Raiders

You are a mining company trying to get rich off rare space gems. Be careful of the giant asteroids, and your ship is haunted!

Use the arrow keys to maneuver on screen. There is also gravity pulling you downward. Dodge the asteroids and collect as many gems as you can. The longer you stay alive, the more points you gain.
