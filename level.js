function getLevelWallHeight(level)
{
    var height = 50;
    return height + level * 5;
}

function getLevelDistance(level)
{
    var distance = 150;
    return distance - level * 5;
}

function getLevelSpeed(level)
{
    var speed = 2;
    return speed * level;
}

function getLevelRocks(level)
{
    var rocks = 40;
    return rocks + (5 * (level + 1));
}

function getLevelDiamonds(level)
{
    var diamonds = 10;
    return diamonds + (5 * (level + 1));
}
